/* Define the counting function prototypes */

int count_words(char* file);
int count_lines(char* file);
int count_characters(char* file);
