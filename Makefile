# Build a Word Counting program

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c counting_functions.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c counting_functions.c 

test:
	wc file.txt -c
	./wc file.txt -c
	wc file.txt -l
	./wc file.txt -l
	wc file.txt -w
	./wc file.txt -w
	wc file.txt --bytes
	./wc file.txt --bytes
	wc file.txt --version
	./wc file.txt --version
	wc file.txt 
	./wc file.txt

clean:
	rm $(TARGET)

