///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Joseph Paragas <joseph60@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date   27_Jan_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "counting_functions.h"

int main(int argc, char* argv[]) 
{

   /* Post error message if there are not enough arguments on the command line */  
   if (argc < 2)
   {

      fprintf(stderr, "USAGE: wc %s\n", argv[0]);
      exit(EXIT_FAILURE);

   } 

   else if (argc == 2)
   {

      printf("%d %d %d [%s]\n",  count_lines(argv[1]), count_words(argv[1]), count_characters(argv[1]), argv[1]);
      exit(1);
   }


   /* Print out the lines, words, or characters depending on the input on the command line*/
   if ((argc == 3 && (strcmp(argv[2], "-l") == 0)) || (strcmp(argv[2], "--lines") == 0))
   {

      printf("%d [%s]\n", count_lines(argv[1]), argv[1]);

   }

   else if ((argc == 3 && (strcmp(argv[2], "-w") == 0)) || (strcmp(argv[2], "--words") == 0))
   {

      printf("%d [%s]\n", count_words(argv[1]), argv[1]);

   }

   else if ((argc == 3 && (strcmp(argv[2], "-c") == 0)) || (strcmp(argv[2], "--bytes") == 0))
   {

      printf("%d [%s]\n", count_characters(argv[1]), argv[1]);

   }

   else if (argc == 3 && strcmp(argv[2], "--version") == 0)
   {
 
      printf("wc: version 2.0\n");
   } 


   return 0;

}
