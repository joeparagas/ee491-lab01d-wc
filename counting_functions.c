///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 02 - WordCount
///
/// @file wc.c
/// @version 2.0
///
/// @author Joseph Paragas <joseph60@hawaii.edu>
/// @brief  Lab 02 - WordCount - EE 491F - Spr 2021
/// @date   27_Jan_2021
/// @info   This file contains the function definitions for the counting 
//          functions.  
///////////////////////////////////////////////////////////////////////////////
#include <ctype.h>
#include <stdio.h>
#include "counting_functions.h"

/* Count the characters in the text file */
int count_characters(char* file)
{
   FILE* fp = fopen(file, "r");
   int count = 0;
   char c;

   if (fp == NULL)
   {

      fprintf(stderr, "[%s] NOT FOUND\n", file);

   }

   while ((c = fgetc(fp)) != EOF)
   {

      count++;

   }

   fclose(fp);
   return count;

}

/* Count the words in the text file */
int count_words(char* file)
{

   FILE* fp = fopen(file, "r");
   int count = 0;
   char c;
   /* Variable that checks the previous character (useful if we have a whitespace) */
   char prev_character = 'x';

   if (fp == NULL)
   {
      fprintf(stderr, "[%s] NOT FOUND\n", file);
   }

   while ((c = fgetc(fp)) != EOF)
   {
  
      if (c ==  ' ' || c == '\n' || c == '\t' || c == '\0')
      {

         /* Add to the total if the previous character was not a white space. */
         if (prev_character != ' '|| prev_character != '\n' || prev_character != '\t' || prev_character != '\0')
         {
            count++;
         
         }

         
      }

      prev_character = c;

   }

   fclose(fp);
   return count;

}

/* Count the lines in the text file */
int count_lines(char* file)
{

   FILE* fp = fopen(file, "r");
   int count = 0;
   char c;

   if (fp == NULL)
   {
      fprintf(stderr, "[%s] NOT FOUND\n", file);
   }

   while ((c = fgetc(fp)) != EOF)
   {

      if (c == '\n')
      {
         count++;
      }
   }

   fclose(fp);
   return count;


}

